import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Indicator 1.0

Page {
    header: PageHeader {
        id: header
        title: i18n.tr("ICE - personal safety app")

        trailingActionBar.actions: [
            Action {
                iconName: 'info'
                text: i18n.tr('About')
                onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'))
            }
        ]
    }

    Settings {
        id: settings

        onSaved: {
            if (!success) {
                message.text = i18n.tr("Failed to save the settings");
                message.color = UbuntuColors.red;
            }
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        clip: true
        contentHeight: contentColumn.height + units.gu(4)

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
                margins: units.gu(2)
            }
            spacing: units.gu(1)

            Label {
                text: i18n.tr("Personal information")
                Layout.fillWidth: true
            }            

            TextArea {
                id: infopersonnal
                Layout.fillWidth: true
                placeholderText: i18n.tr("John Do\n 0123456789 \n2020/01/01")
                Component.onCompleted: text = settings.infopersonnal
                onTextChanged: {
                        settings.infopersonnal = text;
                }
            }
                


            Rectangle { // Spacer
                Layout.preferredHeight: units.gu(1)
            }

            Label {
                text: i18n.tr("Medical information")
                Layout.fillWidth: true
            }    

                TextArea {
                    id: doctorname
                    Layout.fillWidth: true
                    placeholderText: i18n.tr("Doctor : Rose J. 0123456789 \nAllergy : nuts")
                    Component.onCompleted: text = settings.doctorname;
                    onTextChanged: {
                        settings.doctorname = text;
                    }
                }
                
            Rectangle { // Spacer
                Layout.preferredHeight: units.gu(1)
            }

            Label {
                text: i18n.tr("Contact information")
                Layout.fillWidth: true
            }  


                TextArea {
                    id: doctornum
                    Layout.fillWidth: true
                    placeholderText: i18n.tr("Mom : Libert 0123456789 \nDad : Libert 0123456789")
                    Component.onCompleted: text = settings.doctornum;
                    onTextChanged: {
                        settings.doctornum = text;
                    }
                }
                

            



            Label {
                id: message
                visible: false
            }

            Button {
                text: i18n.tr("Save")
                onClicked: {
                    message.visible = true;

                    settings.doctorname = settings.doctorname
                    doctorname.text = doctorname.text

                    settings.doctornum = settings.doctornum
                    doctornum.text = doctornum.text

                    settings.infopersonnal = settings.infopersonnal
                    infopersonnal.text = infopersonnal.text

                    message.text = i18n.tr("Saved the settings, please reboot");
                    message.color = UbuntuColors.green;
                    
                    settings.save();
                }
                color: UbuntuColors.orange
            }

            Button {
                visible: !Indicator.isInstalled

                text: i18n.tr("Install Indicator")
                onClicked: {
                    message.visible = false;
                    Indicator.install();
                }
                color: UbuntuColors.green
            }

            Button {
                visible: Indicator.isInstalled

                text: i18n.tr("Uninstall Indicator")
                onClicked: {
                    message.visible = false;
                    Indicator.uninstall();
                }
            }

            Label {
                textSize: Label.XSmall

                text: i18n.tr('* Before uninstalling the app be sure to uninstall the indicator here first')
            }
        }
    }

    Connections {
        target: Indicator

        onInstalled: {
            message.visible = true;
            if (success) {
                message.text = i18n.tr("Successfully installed, please reboot");
                message.color = UbuntuColors.green;
            }
            else {
                message.text = i18n.tr("Failed to install");
                message.color = UbuntuColors.red;
            }
        }

        onUninstalled: {
            message.visible = true;
            if (success) {
                message.text = i18n.tr("Successfully uninstalled, please reboot");
                message.color = UbuntuColors.green;
            }
            else {
                message.text = i18n.tr("Failed to uninstall");
                message.color = UbuntuColors.red;
            }
        }
    }
}
